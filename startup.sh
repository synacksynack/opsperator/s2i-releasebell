#!/bin/sh

if test "$DEBUG"; then
    set -x
    export DEBUG="*"
else
    export DEBUG="releasebell*"
fi

MYSQL_DB=${MYSQL_DB:-releasebell}
MYSQL_HOST=${MYSQL_HOST:-127.0.0.1}
MYSQL_PASS=${MYSQL_PASS:-secret}
MYSQL_PORT=${MYSQL_PORT:-3306}
MYSQL_USER=${MYSQL_USER:-releasebell}
OPENLDAP_BIND_DN_PREFIX="${OPENLDAP_BIND_DN_PREFIX:-cn=releasebell,ou=services}"
OPENLDAP_BIND_PW="${OPENLDAP_BIND_PW:-secret}"
OPENLDAP_CONF_DN_PREFIX="${OPENLDAP_CONF_DN_PREFIX:-ou=lemonldap,ou=config}"
OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
OPENLDAP_HOST=${OPENLDAP_HOST:-}
OPENLDAP_PROTO=${OPENLDAP_PROTO:-ldap}
OPENLDAP_USERS_OBJECTCLASS=${OPENLDAP_USERS_OBJECTCLASS:-inetOrgPerson}
OPENLDAP_HOST=${OPENLDAP_HOST:-}
OPENLDAP_USERNAME_ATTR=${OPENLDAP_USERNAME_ATTR:-uid}
if test -z "$OPENLDAP_BASE"; then
    OPENLDAP_BASE=`echo "dc=$OPENLDAP_DOMAIN" | sed 's|\.|,dc=|g'`
fi
if test -z "$OPENLDAP_PORT" -a "$OPENLDAP_PROTO" = ldaps; then
    OPENLDAP_PORT=636
elif test -z "$OPENLDAP_PORT"; then
    OPENLDAP_PORT=389
fi

cpt=0
echo Waiting for MySQL backend ...
while true
do
    if echo SHOW TABLES | mysql -u "$MYSQL_USER" \
	    --password="$MYSQL_PASS" -h "$MYSQL_HOST" \
	    -P "$MYSQL_PORT" "$MYSQL_DB" >/dev/null 2>&1; then
	echo " MySQL is alive!"
	break
    elif test "$cpt" -gt 25; then
	echo Could not reach MySQL >&2
	exit 1
    fi
    sleep 5
    echo -n MySQL KO ...
    cpt=`expr $cpt + 1`
done

if test "$OPENLDAP_HOST"; then
    echo Waiting for LDAP backend ...
    cpt=0
    while true
    do
	if LDAPTLS_REQCERT=never \
		ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
		-D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
		-b "ou=users,$OPENLDAP_BASE" \
		-w "$OPENLDAP_BIND_PW" \
		"(objectClass=$OPENLDAP_USERS_OBJECTCLASS)" >/dev/null 2>&1; then
	    echo " LDAP is alive!"
	    export LDAP_URL=$OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT
	    export LDAP_USERS_BASE_DN="ou=users,$OPENLDAP_BASE"
	    export LDAP_BIND_DN="$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE"
	    export LDAP_BIND_PASSWORD="$OPENLDAP_BIND_PW"
	    export LDAP_USERS_OBJECTCLASS=$OPENLDAP_USERS_OBJECTCLASS
	    export LDAP_USERNAME_ATTR=$OPENLDAP_USERNAME_ATTR
	    break
	elif test "$cpt" -gt 25; then
	    echo Could not reach OpenLDAP >&2
	    exit 1
	fi
	sleep 5
	echo LDAP KO ...
	cpt=`expr $cpt + 1`
    done
fi
if test -z "$LDAP_URL"; then
    LOCAL_ADMIN=${LOCAL_ADMIN:-admin}
    LOCAL_PASS=${LOCAL_PASS:-secret}
    cat <<EOF >./users.json
[
    {
	"username": "$LOCAL_ADMIN",
	"password": "$LOCAL_PASS",
	"email": "$LOCAL_ADMIN@$OPENLDAP_DOMAIN"
    }
]
EOF
fi

cat <<EOF >./database.json
{
    "defaultEnv": "k8s",
    "k8s": {
        "host": "$MYSQL_HOST",
        "port": $MYSQL_PORT,
        "user": "$MYSQL_USER",
        "password": "$MYSQL_PASS",
        "database": "$MYSQL_DB",
        "driver": "mysql",
        "multipleStatements": true
    }
}
EOF

if test "$SMTP_HOST"; then
    export MAIL_SMTP_SERVER=$SMTP_HOST \
	MAIL_SMTP_PORT=${SMTP_PORT:-25} \
	MAIL_SMTP_USERNAME= \
	MAIL_SMTP_PASSWORD= \
	MAIL_FROM=releasebell@$OPENLDAP_DOMAIN \
	MAIL_DOMAIN=$OPENLDAP_DOMAIN \
	APP_ORIGIN=$OPENLDAP_DOMAIN
fi
if test -z "$NODE_ENV"; then
    export NODE_ENV=production
fi
if ! ./node_modules/.bin/db-migrate up; then
    echo WARNING: failed initializing database >&2
    exit 1
fi

if test "$GITHUB_TOKEN"; then
    ADMIN_USER=${ADMIN_USER:-admin0}
    if echo "SELECT id FROM users WHERE id = \"$ADMIN_USER\";" \
	    | mysql -u "$MYSQL_USER" --password="$MYSQL_PASS" \
	    -h "$MYSQL_HOST" -P "$MYSQL_PORT" "$MYSQL_DB" \
	    -s 2>/dev/null | grep "$ADMIN_USER" >/dev/null; then
	if ! echo "UPDATE users SET githubToken = \"$GITHUB_TOKEN\" WHERE id = \"$ADMIN_USER;" \
		| mysql -u "$MYSQL_USER" --password="$MYSQL_PASS" \
		-h "$MYSQL_HOST" -P "$MYSQL_PORT" "$MYSQL_DB" \
		2>/dev/null; then
	    echo "Failed updating $ADMIN_USER GitHub token"
	else
	    echo "Successfully updated $ADMIN_USER GitHub token"
	fi
    elif echo "INSERT INTO users VALUES (\"$ADMIN_USER\", \"$ADMIN_USER@$OPENLDAP_DOMAIN\", \"$GITHUB_TOKEN\")" \
	    | mysql -u "$MYSQL_USER" --password="$MYSQL_PASS" \
	    -h "$MYSQL_HOST" -P "$MYSQL_PORT" "$MYSQL_DB" \
	    2>/dev/null; then
	echo "Successfully added $ADMIN_USER user and GitHub token"
    else
	echo "Failed inserting $ADMIN_USER GitHub token"
    fi
fi

unset LOCAL_ADMIN LOCAL_SECRET OPENLDAP_BIND_DN_PREFIX OPENLDAP_BIND_PW \
    OPENLDAP_BASE OPENLDAP_HOST OPENLDAP_PORT OPENLDAP_DOMAIN

exec pm2 start ecosystem.config.js --env production --no-daemon --no-vizion
