// using prometheus-api-metrics
// we dont want to workers responding in the same Pod
//let workersCount = Number(process.env.FORKS || 1);
let workersCount = 1;

module.exports = {
	apps : [
		{
		    name: 'front',
		    script: './index.js',
		    instances: workersCount,
		    env_k8s: { 'NODE_ENV': 'production' },
		    env_production: { 'NODE_ENV': 'production' }
		}
	    ]
    };
