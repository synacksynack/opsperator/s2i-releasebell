#!/usr/bin/env node

'use strict';

if (process.env.AIRBRAKE_ID !== undefined && process.env.AIRBRAKE_ID !== ""
	&& process.env.AIRBRAKE_KEY !== undefined && process.env.AIRBRAKE_KEY !== "") {
    try {
	let airbrake = require('airbrake').createClient(process.env.AIRBRAKE_ID, process.env.AIRBRAKE_KEY);
	airbrake.handleExceptions();
	if (process.env.AIRBRAKE_HOST !== undefined) {
	    console.log(`exceptions would be sent to ${process.env['AIRBRAKE_HOST']}`);
	} else { console.log(`exceptions would be sent to airbrake.io`); }
    } catch(e) {
	console.log(`failed configuring airbrake, caught:`);
	console.log(e);
    }
} else { console.log(`airbrake disabled`); }

require('supererror');

var server = require('./backend/server.js'),
    tasks = require('./backend/tasks.js'),
    database = require('./backend/database.js');

const PORT = process.env.PORT || 3000;

database.init(function (error) {
    if (error) return console.error('Failed to init database.', error);

    server.start(parseInt(PORT), function (error) {
        if (error) return console.error('Failed to start server.', error);

        console.log(`Server is up and running on port ${PORT}`);

        tasks.run();
    });
});
