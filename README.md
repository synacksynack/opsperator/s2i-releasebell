# k8s ReleaseBell

Forked from https://git.cloudron.io/cloudron/releasebell

See `minimal.patch` for an exhaustive overview of how we're patching upstream.
(TODO)

## Quick Start

To contribute, or just test this code on your local station, you may start
services with the following:

```
$ curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
$ nvm install 12.13.0
$ nvm use 12.13.0
$ npm install -g pm2 node-gyp
$ npm install
$ export OPENLDAP_HOST=1.2.3.4 OPENLDAP_BASE=dc=demo,dc=local
$ ./startup.sh
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name              |    Description                         | Default                                                     |
| :---------------------------- | -------------------------------------- | ----------------------------------------------------------- |
|  `ADMIN_USER`                 | Initial user provisioning GitHub token | undef                                                       |
|  `GITHUB_TOKEN`               | Token querying GitHub API              | undef                                                       |
|  `LEMON_PORTAL`               | Toggles LemonLAP Authentication        | undef                                                       |
|  `MYSQL_DB`                   | ReleaseBell MySQL Database             | `codimd`                                                    |
|  `MYSQL_HOST`                 | ReleaseBell MySQL Host                 | `localhost`                                                 |
|  `MYSQL_PASS`                 | ReleaseBell MySQL Password             | `secret`                                                    |
|  `MYSQL_PORT`                 | ReleaseBell MySQL Port                 | `3306`                                                      |
|  `MYSQL_USER`                 | ReleaseBell MySQL User                 | `codimd`                                                    |
|  `OPENLDAP_BASE`              | OpenLDAP Base                          | seds `OPENLDAP_DOMAIN`, default produces `dc=demo,dc=local` |
|  `OPENLDAP_BIND_DN_PREFIX`    | LDAP Bind Prefix                       | `cn=codimd,ou=services`                                     |
|  `OPENLDAP_BIND_PW`           | LDAP Bind Password                     | `secret`                                                    |
|  `OPENLDAP_DOMAIN`            | OpenLDAP Domain Name                   | `demo.local`                                                |
|  `OPENLDAP_PORT`              | LDAP Directory Port                    | `389`                                                       |
|  `OPENLDAP_PROTO`             | LDAP Directory Protocol                | `ldap`                                                      |
|  `OPENLDAP_USERS_OBJECTCLASS` | OpenLDAP Users ObjectClass             | `inetOrgPerson`                                             |
|  `OPENLDAP_USERNAME_ATTR`     | OpenLDAP Username Attribute            | `uid`                                                       |
|  `RELEASEBELL_API_HOSTNAME`   | LemonLDAP-NG Handler Client Hostname   | `localhost`                                                 |
|  `RELEASEBELL_LLNG_BIND_DN`   | LemonLDAP-NG Handler Bind DN           | `cn=ssoapp,ou=services,$OPENLDAP_BASE`                      |
|  `RELEASEBELL_LLNG_BIND_PW`   | LemonLDAP-NG Handler Bind Password     | `secret`                                                    |
|  `RELEASEBELL_LLNG_CONF_DN`   | LemonLDAP-NG Configuratin Base DN      | `ou=lemonldap,ou=config,$OPENLDAP_BASE`                     |
|  `SMTP_HOST`                  | SMTP Relay Address                     | undef                                                       |
|  `SMTP_PORT`                  | SMTP Relay Port                        | `25`                                                        |

You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.
