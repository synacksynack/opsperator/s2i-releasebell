FRONTNAME=opsperator
-include Makefile.cust

.PHONY: install
install:
	@@npm install --unsafe-perm
	@@if ! chmod -R g=u . 2>/dev/null; then \
	    echo ignoring chmod return value; \
	fi

.PHONY: testcontainer
testcontainer:
	@@docker rm -f testreleasebell || true
	@@docker rm -f testmysql || true
	@@docker run --name testmysql \
	    -e MYSQL_DATABASE=msdb \
	    -e MYSQL_PASSWORD=mspassword \
	    -e MYSQL_ROOT_PASSWORD=msrootpassword \
	    -e MYSQL_USER=msuser \
	    -p 3306:3306 \
	    -d docker.io/centos/mariadb-102-centos7:latest
	@@sleep 10
	@@msip=`docker inspect testmysql | awk '/"IPAddress": "/{print $$2}' | head -1 | cut -d'"' -f2`; \
	echo "Working with mysql=$$msip"; \
	docker run --name testreleasebell \
	    -e MYSQL_DB=msdb \
	    -e MYSQL_HOST=$$msip \
	    -e MYSQL_PASS=mspassword \
	    -e MYSQL_USER=msuser \
	    -e NODE_TLS_REJECT_UNAUTHORIZED=0 \
	    -e RELEASEBELL_API_HOSTNAME=rb.demo.local \
	    -d opsperator/releasebell

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task-build task-scan pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in configmap secret service statefulset deployment; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: ocbuild
ocbuild: occheck
	@@oc process -f deploy/openshift/imagestream.yaml | oc apply -f-
	BRANCH=`git rev-parse --abbrev-ref HEAD`; \
	if test "$$GIT_DEPLOYMENT_TOKEN"; then \
	    oc process -f deploy/openshift/build-with-secret.yaml \
		-p "RELEASEBELL_REPOSITORY_REF=$$BRANCH" \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_DEPLOYMENT_TOKEN" \
		| oc apply -f-
	else \
	    oc process -f deploy/openshift/build.yaml \
		-p "RELEASEBELL_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-
	fi

.PHONY: occheck
occheck:
	@@oc whoami >/dev/null 2>&1 || exit 42

.PHONY: occlean
occlean: occheck
	@@oc process -f deploy/openshift/run-persistent.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true
	@@oc process -f deploy/openshift/secret.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true

.PHONY: prep-sso
prep-sso:
	@@if test -s ./lemonldap-ng.ini.tpl -a "$$LEMON_PORTAL"; then \
	    test -z "$$RELEASEBELL_API_HOSTNAME" && RELEASEBELL_API_HOSTNAME=localhost; \
	    RELEASEBELL_SHORT_DOMAINNAME=`echo "$$RELEASEBELL_API_HOSTNAME" | cut -d. -f1`; \
	    RELEASEBELL_REAL_DOMAINNAME=`echo "$$RELEASEBELL_API_HOSTNAME" | sed 's|^[^\.]*\.||'`; \
	    test -z "$$RELEASEBELL_REAL_DOMAINNAME" && RELEASEBELL_REAL_DOMAINNAME=$$OPENLDAP_DOMAIN; \
	    test -z "$$RELEASEBELL_REAL_DOMAINNAME" && RELEASEBELL_REAL_DOMAINNAME=demo.local; \
	    test -z "$$OPENLDAP_BASE" && OPENLDAP_BASE=dc=demo,dc=local; \
	    test -z "$$OPENLDAP_HOST" && OPENLDAP_HOST=localhost; \
	    test -z "$$OPENLDAP_PORT" && OPENLDAP_PORT=389; \
	    test -z "$$OPENLDAP_PROTO" && OPENLDAP_PROTO=ldap; \
	    test -z "$$RELEASEBELL_LLNG_BIND_DN" && RELEASEBELL_LLNG_BIND_DN="cn=ssoapp,ou=services,$$OPENLDAP_BASE"; \
	    test -z "$$RELEASEBELL_LLNG_BIND_PW" && RELEASEBELL_LLNG_BIND_PW=secret; \
	    test -z "$$RELEASEBELL_LLNG_CONF_DN" && RELEASEBELL_LLNG_CONF_DN="ou=lemonldap,ou=config,$$OPENLDAP_BASE"; \
	    sed -e "s|SED_API_HOSTNAME|$$RELEASEBELL_SHORT_DOMAINNAME|g" \
		-e "s|SED_API_DOMAINNAME|$$RELEASEBELL_REAL_DOMAINNAME|g" \
		-e "s|SED_LLNG_BIND_DN|$$RELEASEBELL_LLNG_BIND_DN|" \
		-e "s|SED_LLNG_BIND_PW|$$RELEASEBELL_LLNG_BIND_PW|" \
		-e "s|SED_LLNG_CONF_DN|$$RELEASEBELL_LLNG_CONF_DN|" \
		-e "s|SED_LLNG_BACKEND|$$OPENLDAP_PROTO://$$OPENLDAP_HOST:$$OPENLDAP_PORT|" \
		./lemonldap-ng.ini.tpl >lemonldap-ng.ini; \
	    echo "done initializing link to LLNG"; \
	fi

.PHONY: postinstall
postinstall:
	@@if ! chmod -R g=u . 2>/dev/null; then \
	    echo ignoring chmod return value; \
	fi
	@@./bin/heroku

.PHONY: run
run:	start

.PHONY: start
start: prep-sso
	@@./startup.sh

.PHONY: test
test:
	@@echo no tests
